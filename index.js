// dependencies
const chalk = require('chalk')
const bluebird = require('bluebird')
const clear = require('clear')
const figlet = require('figlet')
const inquirer = require('inquirer')
const redis = require("redis")

// promisifying redis (adding async methods)
bluebird.promisifyAll(redis.RedisClient.prototype)
bluebird.promisifyAll(redis.Multi.prototype)

// creating a redis client
let client = redis.createClient()

// object to store answers locally
let answers

// ================
// ================     FUNCTIONS
// ================

/**
 * @function
 * @name isCached
 * Helper function to check if question has already been answered.
 * @param {string} name - Property name.
 * @param {number} index - If property is an array, its index.
 */
function isCached(name, index=0) {
    if (index) {
        if(answers[name][index]) {
            return true
        }
        return false
    }
    
    if (answers[name]) {
        return true
    }
    
    return false
}

/**
 * @function
 * @name chatbotHeader
 * Helper function to clear console and display an ASCII header.
 */
function chatbotHeader() {
    clear()
    console.log(chalk.white(figlet.textSync('trusk', { horizontalLayout: 'full' })))
}

/**
 * @function
 * @name askQuestion
 * Ask the end-user a question.
 * @param {string} name - Property name.
 * @param {string} type - Answer type expected (string, number...).
 * @param {string} message - Question to be displayed to the end-user.
 * @param {boolean} isArray - If property is an array.
 * @param {number} index - If property is an array, its index.
 */
async function askQuestion(name, type, message, isArray=false, index=0) {
    if (isCached(name, index)) {
        return
    }
    const answer = await inquirer.prompt({
        name: name,
        type: type,
        message: message
    })

    if (!answer[name]) {
        console.log(chalk.red('Nous n\'avons pas compris votre réponse...'))
        await askQuestion(name, type, message, isArray, index)
    }
    else {
        if (isArray) {
            !answers[name] ? answers[name] = [] : null
            answers[name].push(answer[name])
        }
        else {
            answers[name] = answer[name]
        }

        setRedis()
    }
}

/**
 * @function
 * @name showRecap
 * Show a quick recap of all answered questions.
 */
async function showRecap() {
    console.log(`Votre nom est ${chalk.green(answers.truskerName)}`)
    console.log(`Votre société s'appelle ${chalk.green(answers.truskerSocietyName)}`)
    console.log(`Votre société comporte ${chalk.green(answers.truskerSocietyNbEmployees)} employés : `)
    
    for (let i = 0; i < answers.truskerSocietyNbEmployees; i++) {
        console.log(answers.truskerSocietyEmployeesNames[i])
    }

    console.log(`Votre société comporte ${chalk.green(answers.truskerSocietyNbTrucks)} camions : `)

    for (let i = 0; i < answers.truskerSocietyNbTrucks; i++) {
        console.log(`Un ${answers.truskerSocietyTrucksTypes[i]} de ${answers.truskerSocietyTrucksVolumes[i]} mètres cubes`)
    }
}

/**
 * @function
 * @name askConfirmation
 * Ask the end-user to confirm if all answers are valid.
 */
async function askConfirmation() {
    const answer = await inquirer.prompt({
        name: 'confirmation',
        type: 'confirm',
        message: 'Est-ce que tous les renseignements sont corrects ?'
    })
    
    if (answer.confirmation) {
        return true
    }
    else {
        return false
    }
}

/**
 * @function
 * @name clearRedis
 * Clear answers off of Redis server.
 */
async function clearRedis() {
    await client.delAsync('answers')
}

/**
 * @function
 * @name getRedis
 * Retrieve answers off of Redis server and store them locally.
 */
async function getRedis() {
    let cachedAnswers = await client.getAsync('answers')
    answers = JSON.parse(cachedAnswers) || {}
}

/**
 * @function
 * @name setRedis
 * Save answers on Redis Server.
 */
async function setRedis() {
    await client.setAsync('answers', JSON.stringify(answers))
}

/**
 * @function
 * @name onboarding
 * Ask all questions relative to current onboarding.
 */
async function onboarding() {
    await askQuestion('truskerName', 'input', 'Quel est votre nom ?')
    await askQuestion('truskerSocietyName', 'input', 'Quel est le nom de votre société ?')
    await askQuestion('truskerSocietyNbEmployees', 'number', 'Combien d\'employés comporte votre société (en chiffres) ?')

    for (let i = 0; i < answers.truskerSocietyNbEmployees; i++) {
        await askQuestion('truskerSocietyEmployeesNames', 'input', `Comment s'appelle l'employé numéro ${i+1} ?`, true, i)
    }

    await askQuestion('truskerSocietyNbTrucks', 'number', 'Combien de camions possède votre société (en chiffres) ?')

    for (let i = 0; i < answers.truskerSocietyNbTrucks; i++) {
        await askQuestion('truskerSocietyTrucksVolumes', 'number', `Quel est le volume en mètres cubes du camion numéro ${i+1} ?`, true, i)
        await askQuestion('truskerSocietyTrucksTypes', 'input', `De quel type est le camion numéro ${i+1}?`, true, i)
    }
}

/**
 * @function
 * @name main
 * Main program function.
 */
async function main() {

    chatbotHeader()
    await getRedis()
    await onboarding()
    await showRecap()

    let confirmation = await askConfirmation()
    await clearRedis()

    if (!confirmation) {
        console.log('Validation refusée.')
        main()
    }
    else {
        console.log('Validation confirmée.')
        process.exit()
    }
}

main()